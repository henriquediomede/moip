# Builds the report
class WebhookLogReport
  include WebhookLogParser

  def initialize(path)
    @lines = File.read(path).lines
  end

  # analyze the log file, filtering the desired values
  def analyze
    @parse_result = parse_lines(%w(request_to response_status))
    @parse_result.select! { |line_result| line_result }
  end

  def show
    puts
    puts 'TOP 3 WEBHOOKS'
    puts '----------------------------------------------------------'
    top_webhooks.each { |webhook, hits| puts "#{webhook} - #{hits}" }

    puts
    puts 'RESPONSE STAUTS'
    puts '----------------------------------------------------------'
    webhook_status.each { |status, hits| puts "#{status} - #{hits}" }
    puts
  end

  def top_webhooks
    webhooks = @parse_result.map { |v| v['request_to'] }.uniq
    webhooks.map { |i| [i, @parse_result.count { |el| el['request_to'] == i }] }.sort_by(&:last).reverse[0, 3]
  end

  def webhook_status
    webhooks = @parse_result.map { |v| v['response_status'] }.uniq
    webhooks.map { |i| [i, @parse_result.count { |el| el['response_status'] == i }] }.sort
  end
end
