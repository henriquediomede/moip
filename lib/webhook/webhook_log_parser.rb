# Module responsible for parsing the report
# can be reused for different reports and
# not only for the ones created
module WebhookLogParser
  # Parse lines and retrieve the value for each filter
  def parse_lines(filters)
    @lines.map { |line| parse_line(line, filters) }
  end

  private

  def parse_line(line, filters)
    line_result = {}
    filters.each do |filter|
      line_result[filter] = line.slice(/#{Regexp.quote(filter)}([^\s]+)/).tr('"', '').split('=').last
    end
    line_result
  rescue
    nil
  end
end
