require_relative '../lib/webhook'

describe 'WebhookLogReport' do
  describe '#analyze' do
    context 'when file is empty' do
      it 'returns nil' do
        allow(File).to receive(:read).and_return('')
        w = WebhookLogReport.new('')
        expect(w.analyze).to be_nil
      end
    end

    context 'when file has logs' do
      it 'returns [] for a line that is not compatible' do
        allow(File).to receive(:read).and_return('level=info')
        w = WebhookLogReport.new('')
        expect(w.analyze).to eq([])
      end

      it 'returns only filtered fields' do
        allow(File).to receive(:read)
          .and_return("\nlevel=info request_to=\"https://woodenoyster.com.br\" response_status=\"201\"")

        w = WebhookLogReport.new('')

        expect(w.analyze).to eq(
          [{ 'request_to' => 'https://woodenoyster.com.br', 'response_status' => '201' }]
        )
      end

      it 'returns only compatible lines' do
        allow(File).to receive(:read)
          .and_return("\nlevel=info\nrequest_to=\"https://woodenoyster.com.br\" response_status=\"201\"")

        w = WebhookLogReport.new('')

        expect(w.analyze).to eq(
          [{ 'request_to' => 'https://woodenoyster.com.br', 'response_status' => '201' }]
        )
      end

      it 'returns all lines if more than one is compatible' do
        w1 = 'request_to="https://woodenoyster.com.br" response_status="201"'
        w2 = 'request_to="https://woodenoyster.com.br" response_status="400" '

        allow(File).to receive(:read).and_return("\n#{w1}\n#{w2}")
        w = WebhookLogReport.new('')
        expect(w.analyze).to eq(
          [
            { 'request_to' => 'https://woodenoyster.com.br', 'response_status' => '201' },
            { 'request_to' => 'https://woodenoyster.com.br', 'response_status' => '400' }
          ]
        )
      end
    end
  end

  describe '#top_webhooks' do
    context 'when file is empty' do
      it 'returns []' do
        allow(File).to receive(:read).and_return('')
        w = WebhookLogReport.new('')
        w.analyze
        expect(w.top_webhooks).to eq([])
      end
    end

    context 'when file has logs' do
      it 'returns webhooks ordered descending' do
        w = WebhookLogReport.new('spec/fixtures/log.txt')
        w.analyze
        expect(w.top_webhooks).to eq(
          [
            ['https://woodenoyster.com.br', 2],
            ['https://grimpottery.net.br', 2],
            ['https://solidwindshield.net.br', 1]
          ]
        )
      end
    end
  end

  describe '#webhook_status' do
    context 'when file is empty' do
      it 'returns []' do
        allow(File).to receive(:read).and_return('')
        w = WebhookLogReport.new('')
        w.analyze
        expect(w.webhook_status).to eq([])
      end
    end

    context 'when file has logs' do
      it 'returns status ordered by value' do
        w = WebhookLogReport.new('spec/fixtures/log.txt')
        w.analyze
        expect(w.webhook_status).to eq(
          [
            ['200', 1],
            ['400', 2],
            ['404', 1],
            ['500', 2],
            ['503', 1]
          ]
        )
      end
    end
  end
end
