# Desafio moip

## Introdução
O Moip tem uma aplicação que envia webhooks para os ecommerces de seus clientes, esses webhooks possuem informações sobre pagamentos (se foram autorizados, cancelados, etc).

Esta aplicação gera logs bastante grandes, precisamos descobrir através do log quem são os clientes que mais recebem os webhooks e verificar todos o response status retornados pelo servidores dos clientes.

## Task
O arquivo de log em anexo contém informações de envio de webhooks no format:

`
level=info response_body="" request_to"<url>" response_headers= response_status="<code>"
`

Onde:

* url: é a url para onde foi enviado o webhook
* code: é o status code retornado pelo servidor do cliente

As outras informações são irrelevantes para esta task.

Você deve parsear o arquivo e no final mostrar as seguintes informações na saída:

·        3 urls mais chamadas com a quantidade
·        Uma tabela mostrando a quantidade de webhooks por status

Ex:

https://woodenoyster.com.br - 100
https://grotesquemoon.de - 99
https://notoriouslonesome.com - 90

200 - 100
201 - 99

# Solução
A solução proposta pelo exercício busca oferecer o máximo de reusabilidade, permitindo que o mesmo parser seja reaproveitado
em outros reports.

Também foi pensado em deixar os métodos que geram cada indicador separados de forma que seja simples remover ou alterar o relatório.

## Como executar

Primeiramente, instalar todas as depêndencias pelo bundler
`
bundle install
`

`
bundle exec rspec
`

É possível executar o report pelo script através do comando abaixo, sendo o último parâmetro o arquivo desejado
`
ruby bin/main.rb log.txt
`


